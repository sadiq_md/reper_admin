<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('login','Auth\LoginController@sigin');
//Route::post('login','Auth\LoginController@sigin');


Route::get('/login/google/','Auth\LoginController@social')->name('login.google');


Route::get('login/callback','Auth\LoginController@callback');
