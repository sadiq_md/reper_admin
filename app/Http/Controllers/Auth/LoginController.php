<?php


namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{

    public function sigin()
    {
        return view('auth.login');
    }

    public function social()
    {
        return Socialite::driver('google')->redirect();
    }

    public function callback()
    {
        $user = Socialite::driver('google')->user();
        echo '<pre>';print_r($user);
    }
}
